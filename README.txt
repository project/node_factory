
With node_factory a site developer can create nodes on the fly with little PHP knowledge.

function list
- node_factory_create_node : returns an array with nodetype items
- node_factory_set_value : sets a value of an element of the given array
- node_factory_save_node : saves the node
- node_factory_set_cck_value : helper function for CCK values
- node_factory_set_noderef_value : helper function for CCK node reference

Known Issues:
- cck node ref only works for 'Autocomplete' node ref type
- cck checkbox not working yet

Use Case Scenario:
1. You want to create a node 'child' when another node 'parent' is created
2. You want to create a node somewhere somehow.

Solution 1:
a. install workflow_ng and token module
b. create a workflow 'make child by parent' for event 'Content has been created'
c. add a condition 'is parent' and select the 'parent' content type
d. add an action 'create child' of type 'execute PHP code'
e. add the following PHP code to the action. Note the usage of token constructs like [node:title]
<code>
$edit= node_factory_create_node( 'child');

// setting default values
node_factory_set_value( $edit, 'title', "child [node:title]");
node_factory_set_value( $edit, 'body', 'a new child node');

node_factory_save_node( $edit);
</code>
f. create a node parent, submit and see the child node appearing.

Solution 2.
For this scenario I have not found a usage yet.
- Maybe place the number of current logged in users every 5 minute? 
- Or randomly create a 'price node'.
- But this still sound more like 'use workflow_ng' too!!!

a. In this situation the token module, if installed, in not available (yet?) 
   so you need a little more PHP skills
b. add the following code somewhere in your template file
<code>
function my_function_to_create_a_node(){
  $edit= node_factory_create_node( 'my_new_nodetype');

// setting default values
node_factory_set_value( $edit, 'title', "auto created");
node_factory_set_value( $edit, 'body', 'a new child node');

node_factory_save_node( $edit);
</code>
c. provide a trigger somewhere
